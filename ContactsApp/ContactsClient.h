#ifndef CONTACTSCLIENT_H
#define CONTACTSCLIENT_H

#include "common/Contact.h"
#include "utils/ClientSocket.h"
#include <string>

class ContactsClient {
public:
    ContactsClient(const std::string host, int port);
    
    std::vector<uint>* list();
    std::vector<uint>* search( const std::string query );
    void upload( Contact* contact );
    void download( Contact** contact );
    Contact* download( uint id );
    void remove( uint id );
    
    virtual ~ContactsClient();
    
private:
    const std::string* host;
    int port;
};

#endif /* CONTACTSCLIENT_H */

