#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>

class Socket {
public:
    Socket();
    
    bool create();
    bool connect( const std::string host, int port );
    bool send( const char* data, int count );
    int recv( char* buff, int port );
    bool valid();
    void set_non_blocking();
    
    virtual ~Socket();
    
private:
    int sock;
    sockaddr_in addr;
};

#endif /* SOCKET_H */

