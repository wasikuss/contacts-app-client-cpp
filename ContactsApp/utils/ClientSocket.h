#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include "Socket.h"
#include <vector>
#include <algorithm>

class ClientSocket {
public:
    ClientSocket(const std::string host, int port, bool blocking);
    
    void write( const std::vector<char>* data );
    void write(  char value );
    void write( short value);
    void write( int value);
    std::vector<char>* read( int length );
    void flush();
    int readInt();
    
    virtual ~ClientSocket();
    
private:
    Socket* socket;
    std::vector<char>* writeData;
    
    bool isLittleEndian();
};

#endif /* CLIENTSOCKET_H */

