#include "Socket.h"

Socket::Socket() : sock(1) {
    memset(&addr, 0, sizeof addr);
}

bool Socket::create()
{
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    return valid();
}

bool Socket::connect(const std::string host, int port) {
    if( !valid() )
        return false;
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons( port );
    
    int status = inet_pton(addr.sin_family, host.c_str(), &(addr.sin_addr) );
    
    if( status == -1 )
        return false;
    
    status = ::connect( sock, (sockaddr*)(&addr), sizeof addr );
    
    if( errno == EAFNOSUPPORT )
        return false;
    
    return status == 0;
}

bool Socket::send( const char* data, int count )
{
    int status = ::send( sock, data, count, MSG_NOSIGNAL );
    return status != -1;
}

int Socket::recv( char* buff, int lenght ) {
    return ::recv(sock, buff, lenght, 0);
}


void Socket::set_non_blocking()
{
    int opts = fcntl( sock, F_GETFL );
    
    if(opts < 0)
        return;
    
    fcntl( sock, F_SETFL, opts | O_NONBLOCK );
}

bool Socket::valid()
{
    return sock != -1;
}

Socket::~Socket() {
    ::close( sock );
}

