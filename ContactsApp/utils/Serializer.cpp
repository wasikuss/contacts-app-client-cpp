#include "Serializer.h"

Serializer::Serializer() {
   _data = new std::vector<char>();
}

void Serializer::putInt(int value) {
    char* ptr = (char*)&value;
    if( isLittleEndian() )
    {
        _data->insert(_data->end(), ptr+3, ptr+4);
        _data->insert(_data->end(), ptr+2, ptr+3);
        _data->insert(_data->end(), ptr+1, ptr+2);
        _data->insert(_data->end(), ptr, ptr+1);
    }
    else
        _data->insert(_data->end(), ptr, ptr+4);
}

void Serializer::putString(const char* value, int length) {
    _data->insert(_data->end(), value, value+length);
}

std::vector<char>* Serializer::getData() {
    return _data;
}

bool Serializer::isLittleEndian()
{
    short i = 0x0001;
    if( *(char*)&i == 0x01 )
        return true;
    return false;
}

Serializer::~Serializer() {
    delete _data;
}

