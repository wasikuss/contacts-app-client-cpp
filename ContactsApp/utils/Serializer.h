#ifndef SERIALIZER_H
#define SERIALIZER_H

#include <vector>

class Serializer {
public:
    Serializer();
    
    void putInt( int value );
    void putString( const char* value, int length );
    std::vector<char>* getData();
    
    virtual ~Serializer();
    
private:
    std::vector<char>* _data;
    
    bool isLittleEndian();
};

#endif /* SERIALIZER_H */

