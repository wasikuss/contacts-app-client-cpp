#include "ClientSocket.h"

ClientSocket::ClientSocket(const std::string host, int port, bool blocking) {
    socket = new Socket();
    socket->create();
    socket->connect(host, port);
    if( !blocking )
        socket->set_non_blocking();
    writeData = new std::vector<char>();
}

void ClientSocket::write( const std::vector<char>* data) {
    writeData->insert( writeData->end(), data->begin(), data->end() );
}

void ClientSocket::write(char value)
{
    writeData->push_back( value );
}

void ClientSocket::write(short value)
{
    if( isLittleEndian() )
    {
        writeData->push_back( *(((char*)&value) + 1) );
        writeData->push_back( value );
    }
    else
    {
        writeData->insert( writeData->end(), &value, &value + 2 );
    }
}

void ClientSocket::write(int value)
{
    if( isLittleEndian() )
    {
        writeData->push_back( *(((char*)&value) + 3) );
        writeData->push_back( *(((char*)&value) + 2) );
        writeData->push_back( *(((char*)&value) + 1) );
        writeData->push_back( value );
    }
    else
    {
        writeData->insert( writeData->end(), &value, &value + 4 );
    }
}

std::vector<char>* ClientSocket::read( int length ) {
    std::vector<char>* data = new std::vector<char>();
    
    char* buff = (char*)malloc(length);
    socket->recv( buff, length );
    
    data->insert( data->end(), buff, (buff + length) );
    
    free(buff);
    return data;
}

int ClientSocket::readInt() {
    unsigned int value;
    unsigned char* buff = (unsigned char*)malloc(4);
    
    socket->recv( (char*)buff, 4 );
    
    if( isLittleEndian() )
        value = *(buff) << 24 | *(buff+1) << 16 | *(buff+2) << 8 | *(buff+3);
    else
        value = *buff | *(buff+1) << 8 | *(buff+2) << 16 | *(buff+3) << 24;
   
    free(buff);
    return value;
}

void ClientSocket::flush() {
    socket->send( writeData->data(), writeData->size() );
    delete writeData;
    writeData = new std::vector<char>();
}


bool ClientSocket::isLittleEndian()
{
    short i = 0x0001;
    return ( *(char*)&i == 0x01 );
}


ClientSocket::~ClientSocket() {
    delete socket;
}

