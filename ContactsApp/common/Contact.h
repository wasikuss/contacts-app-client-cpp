#ifndef CONTACT_H
#define CONTACT_H

#include "../utils/Serializer.h"
#include <string>
#include <string.h>
#include <map>
#include <vector>
#include <sys/types.h>

typedef std::map<const std::string,const std::string> AdditionalsMap;

class Contact {
public:
    Contact(const std::string name, const std::string surname);
    
    bool setID( uint id );
    int getID();
    const std::string* getName();
    const std::string* getSurname();
    void setName( const std::string name );
    void setSurname( const std::string name );
    std::string getAdditional(const std::string name);
    void putAdditional(const std::string name, const std::string additional);
    const std::vector<std::string>* getAdditionalKeys();
    
    std::vector<char>* serialize();
    
    virtual ~Contact();
    
private:
    uint ID;
    std::string* name;
    std::string* surname;
    AdditionalsMap* additionals;
    std::vector<std::string>* additionalsKeys;
    
    Serializer* serializer;
};

#endif /* CONTACT_H */

