#include <stdlib.h>

#include "Contact.h"

Contact::Contact(const std::string name, const std::string surname) {
    ID = 0;
    this->name = new std::string(name.c_str());
    this->surname = new std::string(surname.c_str());
    additionals = new AdditionalsMap();
    additionalsKeys = new std::vector<std::string>();
}

bool Contact::setID(uint id) {
    if( ID == 0 )
    {
        ID = id;
        return true;
    }
    return false;
}

int Contact::getID() {
    return ID;
}

const std::string* Contact::getName() {
    return name;
}

const std::string* Contact::getSurname() {
    return surname;
}

void Contact::setName(const std::string name) {
    this->name = new std::string(name);
}

void Contact::setSurname(const std::string surname) {
    this->surname = new std::string(surname);
}


void Contact::putAdditional(const std::string name, const std::string additional) {
    additionals->insert(AdditionalsMap::value_type( name, additional ));
    additionalsKeys->push_back(name);
}

std::string Contact::getAdditional(const std::string name) {
    if(additionals->count(name))
        return additionals->at(name);
    else
        return "";
}

const std::vector<std::string>* Contact::getAdditionalKeys()
{
    return additionalsKeys;
}

std::vector<char>* Contact::serialize() {
    if( serializer != NULL )
        delete serializer;
    serializer = new Serializer();
    
    serializer->putInt(ID);
    
    serializer->putInt(name->size());
    serializer->putString(name->data(), name->size());

    serializer->putInt(surname->size());
    serializer->putString(surname->data(), surname->size());
    
    serializer->putInt(additionals->size());
    
    for (AdditionalsMap::iterator it = additionals->begin(); it!=additionals->end(); ++it)
    {
        serializer->putInt(it->first.size());
        serializer->putString(it->first.data(), it->first.size());

        serializer->putInt(it->second.size());
        serializer->putString(it->second.data(), it->second.size());
    }
    
    return serializer->getData();
}


Contact::~Contact() {
    delete additionals, additionalsKeys, serializer;
    serializer = NULL;
}

