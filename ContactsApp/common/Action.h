#ifndef ACTION_H
#define ACTION_H

#ifdef __cplusplus
extern "C" {
#endif

    enum
    {
        ACTION_NULL = 0,
        ACTION_UPLOAD,
        ACTION_DOWNLOAD,
        ACTION_DELETE,
        ACTION_SEARCH
    };


#ifdef __cplusplus
}
#endif

#endif /* ACTION_H */

