#include "ContactsClient.h"
#include "utils/ClientSocket.h"
#include "common/Action.h"
#include <iostream>
#include <curses.h>

ContactsClient::ContactsClient(const std::string host, int port) {
    this->host = new std::string(host);
    this->port = port;
}

std::vector<uint>* ContactsClient::list() {
    return search(*new std::string());
}

std::vector<uint>* ContactsClient::search( const std::string query )
{
    ClientSocket* clientSocket = new ClientSocket(*host, port, true);
    int length = query.length();
    
    clientSocket->write((char)ACTION_SEARCH);
    clientSocket->write((short)length);
    
    if( length > 0 )
    {
        std::vector<char>* data = new std::vector<char>();
        data->insert(data->end(), query.c_str(), query.c_str()+length);
        clientSocket->write(data);
    }
    
    clientSocket->flush();
    
    int count = clientSocket->readInt();
    std::vector<uint>* list = new std::vector<uint>();
    for( int i = 0; i < count; i++ )
        list->push_back(clientSocket->readInt());
    
    delete clientSocket;
    return list;
}

void ContactsClient::upload(Contact* contact) {
    ClientSocket* clientSocket = new ClientSocket(*host, port, true);
    
    clientSocket->write((char)ACTION_UPLOAD);
    clientSocket->write( contact->serialize() );
    clientSocket->flush();
    uint id = clientSocket->readInt();
    
    delete clientSocket;
    contact->setID(id);
}

void ContactsClient::download( Contact** contact )
{
    Contact* contact2 = download((*contact)->getID());
    if( contact2 != NULL )
    {
        delete *contact;
        *contact = contact2;
    }
}

Contact* ContactsClient::download(uint id)
{
    Contact* contact = NULL;
    ClientSocket* clientSocket = new ClientSocket(*host, port, true);
    
    clientSocket->write((char)ACTION_DOWNLOAD);
    clientSocket->write((int)id);
    clientSocket->flush();
    uint id2 = (uint)clientSocket->readInt();
    if( id == id2 )
    {
        int fieldLength = clientSocket->readInt();
        std::vector<char>* data = clientSocket->read(fieldLength);
        std::string* name = new std::string(data->data(), data->data()+data->size());
        delete data;

        fieldLength = clientSocket->readInt();
        data = clientSocket->read(fieldLength);
        std::string* surname = new std::string(data->data(), data->data()+data->size());
        delete data;

        contact = new Contact(*name, *surname);
        delete name, surname;
        
        contact->setID(id);
        
        int additionalsSize = clientSocket->readInt();
        for( int i = 0; i < additionalsSize; i++ )
        {
            fieldLength = clientSocket->readInt();
            data = clientSocket->read(fieldLength);
            std::string* additionalName = new std::string(data->data(), data->data()+data->size());
            delete data;

            fieldLength = clientSocket->readInt();
            data = clientSocket->read(fieldLength);
            std::string* additionalValue = new std::string(data->data(), data->data()+data->size());
            delete data;
            
            contact->putAdditional(*additionalName, *additionalValue);
            //delete additionalName, additionalValue;
        }
    }
    
    delete clientSocket;
    return contact;
}

void ContactsClient::remove(uint id) {
    ClientSocket* clientSocket = new ClientSocket(*host, port, true);
    
    clientSocket->write((char)ACTION_DELETE);
    clientSocket->write((int)id);
    clientSocket->flush();
    
    delete clientSocket;
}

ContactsClient::~ContactsClient() {
    delete host;
}