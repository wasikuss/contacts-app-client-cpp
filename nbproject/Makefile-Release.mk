#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/ContactsApp/ContactsClient.o \
	${OBJECTDIR}/ContactsApp/common/Contact.o \
	${OBJECTDIR}/ContactsApp/utils/ClientSocket.o \
	${OBJECTDIR}/ContactsApp/utils/Serializer.o \
	${OBJECTDIR}/ContactsApp/utils/Socket.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/contactsappcpptest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/contactsappcpptest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/contactsappcpptest ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/ContactsApp/ContactsClient.o: ContactsApp/ContactsClient.cpp 
	${MKDIR} -p ${OBJECTDIR}/ContactsApp
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ContactsApp/ContactsClient.o ContactsApp/ContactsClient.cpp

${OBJECTDIR}/ContactsApp/common/Contact.o: ContactsApp/common/Contact.cpp 
	${MKDIR} -p ${OBJECTDIR}/ContactsApp/common
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ContactsApp/common/Contact.o ContactsApp/common/Contact.cpp

${OBJECTDIR}/ContactsApp/utils/ClientSocket.o: ContactsApp/utils/ClientSocket.cpp 
	${MKDIR} -p ${OBJECTDIR}/ContactsApp/utils
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ContactsApp/utils/ClientSocket.o ContactsApp/utils/ClientSocket.cpp

${OBJECTDIR}/ContactsApp/utils/Serializer.o: ContactsApp/utils/Serializer.cpp 
	${MKDIR} -p ${OBJECTDIR}/ContactsApp/utils
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ContactsApp/utils/Serializer.o ContactsApp/utils/Serializer.cpp

${OBJECTDIR}/ContactsApp/utils/Socket.o: ContactsApp/utils/Socket.cpp 
	${MKDIR} -p ${OBJECTDIR}/ContactsApp/utils
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ContactsApp/utils/Socket.o ContactsApp/utils/Socket.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/contactsappcpptest

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
