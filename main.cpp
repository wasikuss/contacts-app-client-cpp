#include <cstdlib>
#include <iostream>
#include <curses.h>

#include "ContactsApp/ContactsClient.h"

enum 
{
    APPSTATE_START,
    APPSTATE_LIST,
    APPSTATE_SEARCH,
    APPSTATE_ADD,
    APPSTATE_SELECT,
    APPSTATE_MODIFY,
    APPSTATE_EXIT
};

void state_start();
void state_list();
void state_add();
void state_select();
void state_search();
void state_modify();

int modify_index;
int state = APPSTATE_START;
ContactsClient* client;

typedef std::vector<Contact> Contacts;
Contacts* contacts;

int main(int argc, char** argv) {
    client = new ContactsClient("127.0.1.1", 5000);
    
    while(state != APPSTATE_EXIT)
    {
        switch( state )
        {
            case APPSTATE_START:
            {
                state_start();
                break;
            }
            case APPSTATE_LIST:
            {
                state_list();
                break;
            }
            case APPSTATE_ADD:
            {
                state_add();
                break;
            }
            case APPSTATE_SELECT:
            {
                state_select();
                break;
            }
            case APPSTATE_SEARCH:
            {
                state_search();
                break;
            }
            case APPSTATE_MODIFY:
            {
                state_modify();
                break;
            }
        }
    }
    
    delete client, contacts;
    return 0;
}

void state_start()
{
    char buff;

    printf("Contacts App v1.0\n"
        "1. List contacts\n"
        "2. Search contacts\n"
        "3. Add contact\n"
        "0. Exit\n");

    scanf( " %c", &buff );
    switch( buff )
    {
        case '1':
        {
            state = APPSTATE_LIST;
            break;
        }
        case '2':
        {
            state = APPSTATE_SEARCH;
            break;
        }
        case '3':
        {
            state = APPSTATE_ADD;
            break;
        }
        case '0':
        {
            state = APPSTATE_EXIT;
            break;
        }
    }

    printf("\n");
}


void state_list()
{
    char buff;
    std::vector<uint>* list;
    
    contacts = new Contacts();
    
    printf("\n--- List contacts ---\n");
    list = client->list();
    if( list->size() > 0 )
    {
        int i = 1;
        for( std::vector<uint>::iterator it = list->begin(); it < list->end(); ++it )
        {
            Contact* contact = client->download( *it );
            contacts->push_back( *contact );
            printf("[%d]: %d %s %s\n", i++, contact->getID(), contact->getName()->c_str(), contact->getSurname()->c_str());
        }
        
        state = APPSTATE_SELECT;
    }
    else
    {
        printf("NO RESULTS\n0. back to menu\n");

        scanf(" %c", &buff );
        switch( buff )
        {
            case '0':
            {
                state = APPSTATE_START;
                break;
            }
        }
    }

    delete list;
    printf("\n");
}

void state_add()
{
    char* buff;
    std::string *str1, *str2;
    Contact* contact;

    printf("--- Add contact ---\n"
        "Insert name: ");
    scanf(" %m[^\n]%*c", &buff);
    str1 = new std::string(buff);
    free( buff );

    printf("Insert surname: ");
    scanf("%m[^\n]%*c", &buff);
    str2 = new std::string(buff);
    free( buff );

    contact = new Contact( *str1, *str2 );
    delete str1, str2;

    printf("--- additional fields ---\n"
        "Field name ( [enter] to skip): " );
    int status = scanf( "%m[^\n]%*c", &buff);
    while( status == 1 )
    {
        str1 = new std::string(buff);
        free(buff);
        
        printf("Field value: ");
        scanf("%m[^\n]%*c", &buff);
        str2 = new std::string(buff);
        free(buff);
        
        contact->putAdditional( *str1, *str2 );
        delete str1, str2;
        printf("Field name ( [enter] to skip): ");
        status = scanf( "%m[^\n]%*c", &buff);
    }
    free(buff);
    
    client->upload(contact);

    delete contact;
    
    state = APPSTATE_START;
    printf("\n");
}

void state_select()
{
    printf("\n--- Select contact ---\n");
        
    int index;
    printf("0. back to menu\nInsert index: ");
    scanf(" %d", &index );
    
    if( index == 0 )
    {
        state = APPSTATE_START;
        return;
    }
    index--;
    
    //TODO try catch
    Contact* contact = &contacts->at(index);
    printf("Name: %s\nSurname: %s\n", contact->getName()->c_str(), contact->getSurname()->c_str());
    
    const std::vector<std::string>* keys = contact->getAdditionalKeys();
    if( keys->size() > 0 )
    {
        
        for( std::vector<std::string>::const_iterator it = keys->begin(); it < keys->end(); ++it )
        {
            std::string key = *it;
            std::string value = contact->getAdditional(key);
            printf("%s: %s\n", key.c_str(), value.c_str() );
        }
    }
    
    char* buff;
    printf("\n1. Modify\n2. Delete\n0. Back to menu\n");
    scanf(" %mc%*c", &buff );
    switch( *buff )
    {
        case '0':
        {
            state = APPSTATE_START;
            break;
        }
        case '1':
        {
            state = APPSTATE_MODIFY;
            modify_index = index;
            break;
        }
        case '2':
        {
            client->remove(contact->getID());
            state = APPSTATE_START;
            break;
        }
    }
    
    delete contact; 
}

void state_modify()
{
    bool changed = false;
    char* buff;
    std::string *str1, *str2;
    Contact* contact = &contacts->at(modify_index);
    
    printf("\n--- Modify contact ---\nEnter field name to change ( [enter] to skip ): ");
    int status = scanf( "%m[^\n]%*c", &buff);
    while( status == 1 )
    {
        str1 = new std::string(buff);
        free(buff);
        
        printf("Field value: ");
        scanf("%m[^\n]%*c", &buff);
        str2 = new std::string(buff);
        free(buff);
        
        if( strcmp("Name", str1->c_str()) == 0)
        {
            contact->setName( *str2 );
        }
        else if( strcmp("Surname", str1->c_str()) == 0)
        {
            contact->setSurname( *str2 );
        }
        else
        {
            contact->putAdditional( *str1, *str2 );
        }
        
        changed = true;
        
        delete str1, str2;
        printf("\nEnter field name to change ( [enter] to skip): ");
        status = scanf( "%m[^\n]%*c", &buff);
    }
    free(buff);
    
    if( changed )
    {
        client->upload(contact);
    }
    
    delete contact;
    
    state = APPSTATE_START;
    printf("\n");
}

void state_search()
{
    char buff, *buff2;
    std::vector<uint>* list;
    
    contacts = new Contacts();
    
    printf("\n--- Search contacts ---\nSearch: ");
    
    scanf(" %m[^\n]%*c", &buff2);
    std::string* query = new std::string(buff2);
    free(buff2);
    
    list = client->search( *query );
    free(query);
    if( list->size() > 0 )
    {
        int i = 1;
        for( std::vector<uint>::iterator it = list->begin(); it < list->end(); ++it )
        {
            Contact* contact = client->download( *it );
            contacts->push_back( *contact );
            printf("[%d]: %d %s %s", i++, contact->getID(), contact->getName()->c_str(), contact->getSurname()->c_str());
        }
        
        state = APPSTATE_SELECT;
    }
    else
    {
        printf("NO RESULTS\n0. back to menu\n");

        scanf(" %c", &buff );
        switch( buff )
        {
            case '0':
            {
                state = APPSTATE_START;
                break;
            }
        }
    }

    delete list;
    printf("\n");
}